FROM python:3.11.4-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME krit
CMD ["python", "app.py"]
